/*!*******************************************************************************************
 *  \file       behavior_generate_path_with_2d_geometry_map.cpp
 *  \brief      Behavior generate_path_with_2d_geometry_map implementation file.
 *  \details    This file implements the generate_path_with_2d_geometry_map class.
 *  \authors    Rafael Artiñano Muñoz, 
 *              Alberto Rodelgo Perales
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_generate_path_with_2d_geometry_map.h"
#include <pluginlib/class_list_macros.h>

namespace navigation_with_aruco_markers
{
BehaviorGeneratePathWith2DGeometryMap::BehaviorGeneratePathWith2DGeometryMap() : BehaviorExecutionController() 
{ 
  setName("generate_path_with_2d_geometry_map"); 
  setExecutionGoal(ExecutionGoals::ACHIEVE_GOAL);
}

BehaviorGeneratePathWith2DGeometryMap::~BehaviorGeneratePathWith2DGeometryMap() 
{
}

bool BehaviorGeneratePathWith2DGeometryMap::checkSituation()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    setErrorMessage("Error: Drone landed");
    return false;
  }

  return true;
}



void BehaviorGeneratePathWith2DGeometryMap::checkGoal() {

}

void BehaviorGeneratePathWith2DGeometryMap::checkProgress() 
{ 

}

void BehaviorGeneratePathWith2DGeometryMap::checkProcesses() 
{ 

}

void BehaviorGeneratePathWith2DGeometryMap::onConfigure()
{
  nh = getNodeHandle();
  nspace = getNamespace();

  nh.param<std::string>("trajectory_generated_topic",trajectory_generated_str,"droneTrajectoryAbsRefGenerated");
  nh.param<std::string>("mission_point",mission_point_str, "droneMissionPoint");
  nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
  nh.param<std::string>("add_belief",add_belief,"add_belief");
  
  query_client = nh.serviceClient <droneMsgsROS::ConsultBelief> ("/" + nspace + "/" +execute_query_srv);

}

void BehaviorGeneratePathWith2DGeometryMap::onActivate()
{

  first=0;
  path_generated=false;
  is_finished=false;

  mission_point_pub = nh.advertise<droneMsgsROS::dronePositionRefCommand>("/" + nspace + "/"+mission_point_str,10,true);
  trajectory_sub = nh.subscribe("/" + nspace + "/"+trajectory_generated_str,1,&BehaviorGeneratePathWith2DGeometryMap::trajectory_callback,this);
  add_belief_service = nh.serviceClient<droneMsgsROS::AddBelief>("/" + nspace + "/"+add_belief);
  id_gen_client = nh.serviceClient<droneMsgsROS::GenerateID>("/" + nspace + "/belief_manager_process/generate_id");

  std::string arguments = getParameters();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["destination"].IsDefined()){
    std::vector<double> points=config_file["destination"].as<std::vector<double>>();
    target_position.x=points[0];
    target_position.y=points[1];
    target_position.z=points[2];
  }
  else{
      std::cout << "Destination is not defined" << std::endl;
      return;
  }

  estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>("/" + nspace + "/estimated_pose", nh, ros::Duration(1));

  droneMsgsROS::dronePositionRefCommand mission_point;
  mission_point.x=estimated_pose_msg.x;
  mission_point.y=estimated_pose_msg.y;
  mission_point.z=estimated_pose_msg.z;
  mission_point_pub.publish(mission_point);

  mission_point.x=target_position.x;
  mission_point.y=target_position.y;
  mission_point.z=target_position.z;
  mission_point_pub.publish(mission_point);
}

void BehaviorGeneratePathWith2DGeometryMap::onDeactivate()
{
  mission_point_pub.shutdown();
  add_belief_service.shutdown();
  id_gen_client.shutdown();
}


void BehaviorGeneratePathWith2DGeometryMap::onExecute() 
{ 
  if(is_finished==false)
  {
    if(path_generated)
    {
      BehaviorExecutionController::setTerminationCause(aerostack_msgs::BehaviorActivationFinished::GOAL_ACHIEVED);
      std::cout << "GOAL ACHIEVED" << std::endl;
      is_finished = true;
      return;
    }
}

}

void BehaviorGeneratePathWith2DGeometryMap::trajectory_callback(const droneMsgsROS::dronePositionTrajectoryRefCommand & msg)
{
  if(!path_generated){
    droneMsgsROS::AddBelief add_belief_msg;
    int id = requestBeliefId();
    std::ostringstream ret_stream;
    ret_stream << std::setprecision(2) << std::fixed;
    ret_stream << "path(" << id << ",(";
    for(int i=0;i<msg.droneTrajectory.size();i++){
      ret_stream << "(";
      ret_stream << msg.droneTrajectory[i].x << ",";
      ret_stream << msg.droneTrajectory[i].y << ",";
      ret_stream << msg.droneTrajectory[i].z;
      ret_stream << ")" << (i < msg.droneTrajectory.size() -1 ? ", " : "");
    }
      ret_stream << "))";
      add_belief_msg.request.multivalued = false;
      add_belief_msg.request.belief_expression = ret_stream.str();
      std::cout<< ret_stream.str() << std::endl;
      add_belief_service.call(add_belief_msg);
      std::ostringstream str_obj;
      str_obj <<"object("<<id<<", path)";
      add_belief_msg.request.belief_expression =str_obj.str();
      add_belief_service.call(add_belief_msg);
      path_generated=true;
  }
}
int BehaviorGeneratePathWith2DGeometryMap::requestBeliefId(){
  int ret = 100;
  droneMsgsROS::GenerateID::Request req;
  droneMsgsROS::GenerateID::Response res;
  
  id_gen_client.call(req, res);

  if( res.ack ){
    ret = res.id;
  }
  return ret;
}

}
PLUGINLIB_EXPORT_CLASS(navigation_with_aruco_markers::BehaviorGeneratePathWith2DGeometryMap, nodelet::Nodelet)
