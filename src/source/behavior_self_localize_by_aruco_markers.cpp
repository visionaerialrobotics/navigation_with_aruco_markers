/*!*******************************************************************************************
 *  \file       behavior_self_localize_by_aruco_markers.cpp
 *  \brief      Behavior self_localize_by_aruco_markers implementation file.
 *  \details    This file implements the self_localize_by_aruco_markers class.
 *  \authors    Rafael Artiñano Muñoz, 
 *              Alberto Rodelgo Perales
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_self_localize_by_aruco_markers.h"
#include <pluginlib/class_list_macros.h>

namespace navigation_with_aruco_markers
{
BehaviorSelfLocalizeByArucoMarkers::BehaviorSelfLocalizeByArucoMarkers() : BehaviorExecutionController() 
{ 
  setName("self_localize_by_aruco_markers"); 
  setExecutionGoal(ExecutionGoals::KEEP_RUNNING);
}

BehaviorSelfLocalizeByArucoMarkers::~BehaviorSelfLocalizeByArucoMarkers() 
{
}

bool BehaviorSelfLocalizeByArucoMarkers::checkSituation()
{
  return true;
}



void BehaviorSelfLocalizeByArucoMarkers::checkGoal() {

}

void BehaviorSelfLocalizeByArucoMarkers::checkProgress() 
{ 

}

void BehaviorSelfLocalizeByArucoMarkers::checkProcesses() 
{ 

}

void BehaviorSelfLocalizeByArucoMarkers::onConfigure()
{
  nh = getNodeHandle();
  nspace = getNamespace();
  
  // Start processes
  request_processes_activation_cli =
      nh.serviceClient<aerostack_msgs::RequestProcesses>("/" + nspace + "/" + "start_processes");
  request_processes_deactivation_cli =
      nh.serviceClient<aerostack_msgs::RequestProcesses>("/" + nspace + "/" + "stop_processes");

  request_processes_srv.request.processes = {"droneObstacleDistanceCalculator", "drone_aruco_eye_ros_module",
                                             "visual_markers_localizer_process","droneObstacleProcessor"};

}

void BehaviorSelfLocalizeByArucoMarkers::onActivate()
{

  ros::ServiceClient start_controller1 = nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneObstacleDistanceCalculator/start");
  ros::ServiceClient start_controller2 = nh.serviceClient<std_srvs::Empty>("/"+nspace+"/drone_aruco_eye_ros_module/start");
  ros::ServiceClient start_controller3 = nh.serviceClient<std_srvs::Empty>("/"+nspace+"/visual_markers_localizer_process/start");
  ros::ServiceClient start_controller4 = nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneObstacleProcessor/start");
  std_srvs::Empty req1,req2,req3,req4;
  start_controller1.call(req1);
  start_controller2.call(req2);
  start_controller3.call(req3);
  start_controller4.call(req4);

  nh.param<std::string>("visual_marker_str",visual_marker_str,"change_self_localization_mode_to_visual_markers");
  visual_marker_srv = nh.serviceClient<std_srvs::Empty>("/"+nspace+"/"+visual_marker_str);
  std_srvs::Empty empty;
  std::cout << "Change mode to visual markers" << std::endl;
  visual_marker_srv.call(empty);
}

void BehaviorSelfLocalizeByArucoMarkers::onDeactivate()
{
  visual_marker_srv.shutdown();
}


void BehaviorSelfLocalizeByArucoMarkers::onExecute() 
{ 

}

}
PLUGINLIB_EXPORT_CLASS(navigation_with_aruco_markers::BehaviorSelfLocalizeByArucoMarkers, nodelet::Nodelet)
