/*!*******************************************************************************************
 *  \file       BEHAVIOR_SELF_LOCALIZE_BY_ARUCO_MARKERS.h
 *  \brief      This file contains the self_localize_by_aruco_markers declaration. To obtain more information about
 *              it's definition consult the BEHAVIOR_SELF_LOCALIZE_BY_ARUCO_MARKERS.cpp file.
 *  \authors    Rafael Artiñano Muñoz,
 *              Alberto Rodelgo Perales
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid. All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef BEHAVIOR_SELF_LOCALIZE_BY_ARUCO_MARKERS_H
#define BEHAVIOR_SELF_LOCALIZE_BY_ARUCO_MARKERS_H

// ROS
#include <ros/ros.h>
#include "std_srvs/Empty.h"

#include <aerostack_msgs/BehaviorEvent.h>
#include <aerostack_msgs/RequestProcesses.h>
#include <behavior_execution_controller.h>


namespace navigation_with_aruco_markers
{

class BehaviorSelfLocalizeByArucoMarkers:public BehaviorExecutionController
{

public:
  BehaviorSelfLocalizeByArucoMarkers();
  ~BehaviorSelfLocalizeByArucoMarkers();
private:

  std::string visual_marker_str;

  ros::ServiceClient visual_marker_srv;
  ros::NodeHandle nh;
  std::string nspace;

  ros::ServiceClient request_processes_activation_cli;
  ros::ServiceClient request_processes_deactivation_cli;
  aerostack_msgs::RequestProcesses request_processes_srv;
  
  void onConfigure();
  void onActivate();
  void onDeactivate();
  void onExecute();
  bool checkSituation();
  void checkGoal();
  void checkProgress();
  void checkProcesses();
};
}

#endif
