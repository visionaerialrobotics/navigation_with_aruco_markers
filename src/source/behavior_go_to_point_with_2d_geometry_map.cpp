/*!*******************************************************************************************
 *  \file       behavior_go_to_point_with_2d_geometry_map.cpp
 *  \brief      Behavior go_to_point_with_2d_geometry_map implementation file.
 *  \details    This file implements the go_to_point_with_2d_geometry_map class.
 *  \authors    Rafael Artiñano Muñoz, 
 *              Alberto Rodelgo Perales
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_go_to_point_with_2d_geometry_map.h"
#include <pluginlib/class_list_macros.h>

namespace navigation_with_aruco_markers
{
BehaviorGoToPointWith2DGeometryMap::BehaviorGoToPointWith2DGeometryMap() : BehaviorExecutionController() 
{ 
  setName("go_to_point_with_2d_geometry_map"); 
  setExecutionGoal(ExecutionGoals::ACHIEVE_GOAL);
}

BehaviorGoToPointWith2DGeometryMap::~BehaviorGoToPointWith2DGeometryMap() 
{
}

bool BehaviorGoToPointWith2DGeometryMap::checkSituation()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    setErrorMessage("Error: Drone landed");
    return false;
  }

  return true;
}



void BehaviorGoToPointWith2DGeometryMap::checkGoal() {

}

void BehaviorGoToPointWith2DGeometryMap::checkProgress() 
{ 

}

void BehaviorGoToPointWith2DGeometryMap::checkProcesses() 
{ 

}

void BehaviorGoToPointWith2DGeometryMap::onConfigure()
{
  nh = getNodeHandle();
  nspace = getNamespace();

  nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  nh.param<std::string>("estimated_speed_topic",estimated_speed_str,"estimated_speed");
  nh.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
  nh.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
  nh.param<std::string>("mission_point",mission_point, "droneMissionPoint");
  nh.param<std::string>("droneTrajectoryAbsRefCommand_topic", trajectory_ref_command_str, "droneTrajectoryAbsRefCommand");
  nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");

  query_client = nh.serviceClient <droneMsgsROS::ConsultBelief> ("/"+nspace+"/"+execute_query_srv);
}

void BehaviorGoToPointWith2DGeometryMap::onActivate()
{
  ros::ServiceClient start_controller = nh.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryController/start");
  std_srvs::Empty req;
  start_controller.call(req);

  is_finished = false;
  /*Initialize topics*/
  estimated_pose_sub = nh.subscribe("/"+nspace+"/"+estimated_pose_str, 1000, &BehaviorGoToPointWith2DGeometryMap::estimatedPoseCallBack, this);
  estimated_speed_sub = nh.subscribe("/"+nspace+"/"+estimated_speed_str, 1000, &BehaviorGoToPointWith2DGeometryMap::estimatedSpeedCallback, this);

  controllers_pub = nh.advertise<droneMsgsROS::droneCommand>("/"+nspace+"/"+controllers_str, 10, true);
  yaw_controller_pub=nh.advertise<droneMsgsROS::droneYawRefCommand>("/"+nspace+"/"+yaw_controller_str,10, true);
  mode_service = nh.serviceClient<droneMsgsROS::setControlMode>("/"+nspace+"/"+service_topic_str);
  mission_point_pub = nh.advertise<droneMsgsROS::dronePositionRefCommand>("/"+nspace+"/"+mission_point,10, true);
  reference_trajectory_pub = nh.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("/"+nspace+"/"+trajectory_ref_command_str,10, true);

  estimated_pose_msg.x=0;
  estimated_pose_msg.y=0;
  estimated_pose_msg.z=0;
  estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>("/"+nspace+"/"+estimated_pose_str, nh);


  // Extract target position
  std::string arguments = getParameters();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["coordinates"].IsDefined()){
    std::vector<double> points=config_file["coordinates"].as<std::vector<double>>();
    target_position.x=points[0];
    target_position.y=points[1];
    target_position.z=points[2];
  }
  else{
    if(config_file["relative_coordinates"].IsDefined())
    {
      std::vector<double> points=config_file["relative_coordinates"].as<std::vector<double>>();
      target_position.x=estimated_pose_msg.x+points[0]+0.0001;
      target_position.y=estimated_pose_msg.y+points[1];
      target_position.z=estimated_pose_msg.z+points[2];
    }
    else{
      std::cout << "Coordinates are not defined" << std::endl;
      return;
    }
  }
   // Publish controller reference
   droneMsgsROS::dronePositionTrajectoryRefCommand reference_trajectory_msg;
   droneMsgsROS::dronePositionRefCommand reference_position_msg;
   reference_trajectory_msg.header.frame_id="GO_TO_POINT";
   reference_trajectory_msg.header.stamp= ros::Time::now();
   reference_position_msg.x = estimated_pose_msg.x;
   reference_position_msg.y = estimated_pose_msg.y;
   reference_position_msg.z = estimated_pose_msg.z;
   reference_trajectory_msg.droneTrajectory.push_back(reference_position_msg);
   reference_trajectory_pub.publish(reference_trajectory_msg);

   reference_position_msg.x = target_position.x;
   reference_position_msg.y = target_position.y;
   reference_position_msg.z = target_position.z;
   reference_trajectory_msg.droneTrajectory.push_back(reference_position_msg);
   reference_trajectory_pub.publish(reference_trajectory_msg);

   //Yaw reference
   droneMsgsROS::droneYawRefCommand reference_yaw_msg;
   reference_yaw_msg.yaw = estimated_pose_msg.yaw;
   reference_yaw_msg.header.stamp = ros::Time::now();
   yaw_controller_pub.publish(reference_yaw_msg);

  // Send target point
   droneMsgsROS::dronePositionRefCommand mission_point;
   mission_point.x=estimated_pose_msg.x;
   mission_point.y=estimated_pose_msg.y;
   mission_point.z=estimated_pose_msg.z;
   mission_point_pub.publish(mission_point);

  // Send target point
  mission_point.x=target_position.x;
  mission_point.y=target_position.y;
  mission_point.z=target_position.z;
  mission_point_pub.publish(mission_point);
  
  //MOVE command
  droneMsgsROS::droneCommand msg;
  msg.header.frame_id = "behavior_go_to_point";
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);
}

void BehaviorGoToPointWith2DGeometryMap::onDeactivate()
{
  estimated_pose_sub.shutdown();
  estimated_speed_sub.shutdown();
  controllers_pub.shutdown();
  yaw_controller_pub.shutdown();
  mode_service.shutdown();
  mission_point_pub.shutdown();  
  reference_trajectory_pub.shutdown();
}


void BehaviorGoToPointWith2DGeometryMap::onExecute() 
{ 

 float pose_variation_maximum=0.2;
  float speed_maximum_at_end=0.1;
  if(is_finished==false)
  {
    if(std::abs(target_position.z-estimated_pose_msg.z)<pose_variation_maximum &&
    std::abs(target_position.x-estimated_pose_msg.x)<pose_variation_maximum &&
    std::abs(target_position.y- estimated_pose_msg.y)<pose_variation_maximum)
    {
      BehaviorExecutionController::setTerminationCause(aerostack_msgs::BehaviorActivationFinished::GOAL_ACHIEVED);
      is_finished = true;
      std::cout << "GOAL ACHIEVED" << std::endl;
      return;
    }
  }


}

void BehaviorGoToPointWith2DGeometryMap::estimatedSpeedCallback(const droneMsgsROS::droneSpeeds& msg)
{
  estimated_speed_msg=msg;
}
void BehaviorGoToPointWith2DGeometryMap::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
  estimated_pose_msg=msg;
}


}
PLUGINLIB_EXPORT_CLASS(navigation_with_aruco_markers::BehaviorGoToPointWith2DGeometryMap, nodelet::Nodelet)
