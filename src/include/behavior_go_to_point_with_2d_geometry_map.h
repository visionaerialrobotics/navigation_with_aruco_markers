/*!*******************************************************************************************
 *  \file       BEHAVIOR_GO_TO_POINT_WITH_2D_GEOMETRY_MAP.h
 *  \brief      This file contains the go_to_point_with_2d_geometry_map declaration. To obtain more information about
 *              it's definition consult the BEHAVIOR_GO_TO_POINT_WITH_2D_GEOMETRY_MAP.cpp file.
 *  \authors    Rafael Artiñano Muñoz,
 *              Alberto Rodelgo Perales
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid. All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef BEHAVIOR_GO_TO_POINT_WITH_2D_GEOMETRY_MAP_H
#define BEHAVIOR_GO_TO_POINT_WITH_2D_GEOMETRY_MAP_H

// ROS
#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include <droneMsgsROS/droneSpeeds.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <yaml-cpp/yaml.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <aerostack_msgs/RequestProcesses.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/setControlMode.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <tuple>
// Aerostack msgs
#include <aerostack_msgs/BehaviorEvent.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
//Aerostack libraries

#include <behavior_execution_controller.h>


namespace navigation_with_aruco_markers
{

class BehaviorGoToPointWith2DGeometryMap:public BehaviorExecutionController
{

public:
  BehaviorGoToPointWith2DGeometryMap();
  ~BehaviorGoToPointWith2DGeometryMap();
private:

  //Congfig variables
  std::string estimated_pose_str;
  std::string controllers_str;
  std::string estimated_speed_str;
  std::string yaw_controller_str;
  std::string service_topic_str;
  std::string drone_position_str;
  std::string mission_point;
  std::string trajectory_ref_command_str;
  std::string execute_query_srv;

  ros::ServiceClient request_processes_activation_cli;
  ros::ServiceClient request_processes_deactivation_cli;
  aerostack_msgs::RequestProcesses request_processes_srv;

  //Subscriber---
  ros::Subscriber estimated_pose_sub;
  ros::Subscriber estimated_speed_sub;
  ros::Publisher controllers_pub;
  ros::Publisher yaw_controller_pub;
  ros::ServiceClient mode_service;
  ros::ServiceClient query_client;

  //Message
  droneMsgsROS::dronePose estimated_pose_msg;
  droneMsgsROS::dronePose target_position;
  droneMsgsROS::droneSpeeds estimated_speed_msg;

  ros::NodeHandle nh;
  std::string nspace;
  ros::ServiceClient id_gen_client;
  ros::Publisher reference_trajectory_pub;
  ros::Publisher mission_point_pub;
  ros::Subscriber trajectory_pub;
  ros::ServiceClient add_belief_service;

  bool is_finished;

  void onConfigure();
  void onActivate();
  void onDeactivate();
  void onExecute();
  bool checkSituation();
  void checkGoal();
  void checkProgress();
  void checkProcesses();

  void estimatedPoseCallBack(const droneMsgsROS::dronePose&);
  void estimatedSpeedCallback(const droneMsgsROS::droneSpeeds&);
};
}

#endif

